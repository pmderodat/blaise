(* Abstract the stream concept from the file/buffer implementations for JVM binary
* outputs (ie. big endian byte order). *)

type out_stream

val stream_from_channel : out_channel -> out_stream
val stream_from_buffer : Buffer.t -> out_stream

val out8 : out_stream -> int -> unit
val out16 : out_stream -> int -> unit
val out32 : out_stream -> Int32.t -> unit
val out64 : out_stream -> Int64.t -> unit

val out_string : out_stream -> string -> unit
val transfer_buffer : out_stream -> Buffer.t -> unit
