(* This module compiles the Blaise AST to JVM bytecode. *)

exception InvalidFilename of string * string

val compile : string -> Ast.prg -> out_channel -> unit
