(* This module compiles the Blaise AST to JVM bytecode. *)

exception InvalidFilename of string * string

let out8, out16, out32, out64 = (
  Utils.out8, Utils.out16, Utils.out32, Utils.out64)
let out_string = Utils.out_string

type major_version =
  | J2SE7
  | J2SE6
  | J2SE5


type access =
  | Public
  | Private
  | Protected
  | Static
  | Final
  | Super
  | Synchronized
  | Bridge
  | Volatile
  | Transient
  | Varargs
  | Native
  | Interface
  | Abstract
  | Strict
  | Synthetic
  | Annotation
  | Enum


(* Type descriptors are used in fields/methods/attributes descriptors *)
type type_descriptor =
  | TypeByte
  | TypeChar
  | TypeDouble
  | TypeFloat
  | TypeInt
  | TypeLong
  | TypeClassInstance of string
  | TypeShort
  | TypeBoolean
  | TypeArray of type_descriptor
  | TypeMethod of type_descriptor list * type_descriptor
  | TypeVoid


(* Data type for constants as referenced by the code. *)
type code_constant =
  | CString of string
  | CInteger of Int32.t
  | CFloat of float
  | CLong of Int64.t
  | CDouble of float
    (* Name of the class *)
  | CClass of string
  | CStringRef of string
    (* Name of the class * field name * field type *)
  | CFieldRef of string * string * string
    (* Name of the class * method name * method type *)
  | CMethodRef of string * string * string
    (* Name of the interface * member name * member type *)
  | CInterfaceMethodRef of string * string * string
    (* Unqualified member name * type descriptor *)
  | CNameAndType of string * string

(* Intermediate data type for constants before to be put in the class file
 * constants pool. This one is used to add references (integer indicies into the
 * pool) between pool elements. *)
type pool_constant =
  | PString of string
  | PInteger of Int32.t
  | PFloat of float
  | PLong of Int64.t
  | PDouble of float
  | PClass of int
  | PStringRef of int
  | PFieldRef of int * int
  | PMethodRef of int * int
  | PInterfaceMethodRef of int * int
  | PNameAndType of int * int


(* The constant_pool_builder class needs a Map for code_constant values. Thus,
 * we need to define an OrderedType module in order to create such a map. *)
module OrderedCodeConstant = struct
  type t = code_constant
  let compare = Pervasives.compare
end
module ConstMap = Map.Make (OrderedCodeConstant)

(* Constant pool builder: register constants for the pool. If some code needs a
 * constant, it just has to call the `get` method and give it a `code constant`
 * value to get back a suitable pool index value. Later, in the output phase,
 * there is just to output registered contants one by one to get a correct
 * constants pool. *)
class constant_pool_builder = object (self)
  (* To avoid any duplicate in the constant pool, we use a map to keep in
   * memory already registered constants *and* their associated pool
   * indices. *)
  val mutable registry = ConstMap.empty
  val pool = Queue.create ()
  (* Accoding to the specification, the first pool index is 1 *)
  val mutable next_pool_index = 1

  method length = next_pool_index

  method get code_const =
    try
      (* Return the correct pool index if this constant has already been
       * registered. *)
      ConstMap.find code_const registry
    with Not_found -> (
      (* Otherwise, register recursively its components, register the constant
       * itself and finally return the pool index. *)
      let new_index =
        let resolve_member class_name name type_ = (
          self#get (CClass class_name),
          self#get (CNameAndType (name, type_))
        )
        in
        match code_const with
          | CString str -> self#register (PString str)
          | CInteger i -> self#register (PInteger i)
          | CFloat f -> self#register (PFloat f)
          | CLong l -> self#register (PLong l)
          | CDouble d -> self#register (PDouble d)

          | CClass name ->
              let name_idx = self#get (CString name) in
              self#register (PClass name_idx)
          | CStringRef str ->
              let str_idx = self#get (CString str) in
              self#register (PStringRef str_idx)
          | CNameAndType (name, type_) ->
              let name_idx = self#get (CString name)
              and type_idx = self#get (CString type_)
              in
              self#register (PNameAndType (name_idx, type_idx))

          | CFieldRef (cls, name, type_) ->
              let cls_idx, descr_idx = resolve_member cls name type_ in
              self#register (PFieldRef (cls_idx, descr_idx))
          | CMethodRef (cls, name, type_) ->
              let cls_idx, descr_idx = resolve_member cls name type_ in
              self#register (PMethodRef (cls_idx, descr_idx))
          | CInterfaceMethodRef (cls, name, type_) ->
              let cls_idx, descr_idx = resolve_member cls name type_ in
              self#register (PInterfaceMethodRef (cls_idx, descr_idx))
      in
      registry <- ConstMap.add code_const new_index registry;
      new_index
    )

  (* Append the given pool constant, update the next pool index and return the
   * given pool constant's pool index. *)
  method private register pool_const =
    (* According to the specitifation, long an double pool constants take two
     * pool slots, the other kind of constants only take one. *)
    let index_increment = match pool_const with
      | PLong _ | PDouble _ -> 2
      | _ -> 1
    and new_pool_index = next_pool_index
    in
    Queue.push pool_const pool;
    next_pool_index <- new_pool_index + index_increment;
    new_pool_index

  (* Constant pool output is going to use this method to browse pool constants
  * in the correct order (this is why we use a Queue.t). *)
  method iter f = Queue.iter f pool
end


(* Address for an instruction operand *)
type addr_operand =
    (* Used for resolved addresses *)
  | OpOffset of int
     (* Used for unresolved addresses. The int parameter act as a label: think
      * of `OpLabel 1` is like "L1:" we can see in assembly code. *)
  | OpLabel of int

type instruction =
  | AAStore
  | ALoad_0
  | ANewArray of int
  | Dup
  | GetStatic of int
  | Goto of addr_operand
  | IAdd
  | IConst_0
  | IConst_1
  | IDiv
  | If_ICmpEq of addr_operand
  | If_ICmpGe of addr_operand
  | If_ICmpGt of addr_operand
  | If_ICmpLe of addr_operand
  | If_ICmpLt of addr_operand
  | IfEq of addr_operand
  | IfNe of addr_operand
  | ILoad of int
  | ILoad_0
  | IMul
  | INeg
  | InvokeSpecial of int
  | InvokeStatic of int
  | InvokeVirtual of int
  | IReturn
  | IStore of int
  | ISub
  | Ldc of int
  | Pop
  | Return


(* TODO: fix this data type to properly handle interfaces... if needed. *)
type interface = INTERFACE

type attribute_code =
    {
      co_max_stack : int;
      co_max_locals : int;
      co_code_length : int;
      co_code : instruction list;
      (* TODO: complete (if needed?) with exception table *)
      co_attr : attribute list;
    }

and attribute_info =
    AttrCode of attribute_code

and attribute =
    {
      attr_name_idx : int;
      attr_info : attribute_info;
    }

type classmember =
    {
      m_access : access list;
      m_name_idx : int;
      m_descr_idx : int;
      m_attributes : attribute list;
    }

type classfile =
    {
      version : major_version * int;
      const_pool : constant_pool_builder;
      class_access : access list;
      this_class : int;
      super_class : int;
      interfaces : interface list;
      fields : classmember list;
      methods : classmember list;
      class_attributes: attribute list;
    }


(*
 * Utilities to convert elementary data types for the class file to atomic
 * types.
 *)

(* Convert the access list to a byte. *)
let major_to_short = function
  | J2SE7 -> 0x33
  | J2SE6 -> 0x32
  | J2SE5 -> 0x31

(* Convert the access list to a 16-bits bitmask. *)
let access_to_short =
  (* Each access type is a bit, thus we just have to OR all bits. *)
  List.fold_left (fun accu access -> accu lor (match access with
    | Public ->     0x0001
    | Private ->    0x0002
    | Protected ->  0x0004
    | Static ->     0x0008
    | Final ->      0x0010
    | Super ->      0x0020
    | Synchronized -> 0x0020
    | Bridge ->     0x0040
    | Volatile ->   0x0040
    | Transient ->  0x0080
    | Varargs ->    0x0080
    | Native ->     0x0100
    | Interface ->  0x0200
    | Abstract ->   0x0400
    | Strict ->     0x0800
    | Synthetic ->  0x1000
    | Annotation -> 0x2000
    | Enum ->       0x4000
  )) 0 (* Start with the lor-neutral element: 0 *)

(* Serialize a type descriptor (that is used for fields and methods). *)
let rec type_descriptor_to_string = function
  | TypeByte ->     "B"
  | TypeChar ->     "C"
  | TypeDouble ->   "D"
  | TypeFloat ->    "F"
  | TypeInt ->      "I"
  | TypeLong ->     "L"
  | TypeShort ->    "S"
  | TypeBoolean ->  "Z"
  | TypeVoid ->     "V"
  | TypeClassInstance classname ->
      Printf.sprintf "L%s;" classname
  | TypeArray type_descr ->
      Printf.sprintf "[%s" (type_descriptor_to_string type_descr)
  | TypeMethod (args_type, return_type) ->
      Printf.sprintf "(%s)%s"
        (String.concat "" (List.map type_descriptor_to_string args_type))
        (type_descriptor_to_string return_type)

(* Return the byte length of the given instruction in its binary form. This is
 * used to compute the size of code blocks, and thus to resolve address
 * operands. *)
let get_instruction_length = function
  | AAStore -> 1
  | ALoad_0 -> 1
  | ANewArray _ -> 3
  | Dup -> 1
  | GetStatic _ -> 3
  | Goto _ -> 3
  | IAdd -> 1
  | IConst_0 | IConst_1 -> 1
  | IDiv -> 1

  | If_ICmpLe _ | If_ICmpLt _ | If_ICmpGe _ | If_ICmpGt _
  | If_ICmpEq _ | IfEq _ | IfNe _ ->
      3
  | ILoad _ -> 2
  | ILoad_0 -> 1
  | IMul -> 1
  | INeg -> 1
  | InvokeSpecial _ -> 3
  | InvokeStatic _ -> 3
  | InvokeVirtual _ -> 3
  | IReturn -> 1
  | IStore _ -> 2
  | ISub -> 1
  | Ldc _ -> 2
  | Pop -> 1
  | Return -> 1

let output_instruction output =
  let get_offset = function
    | OpOffset off -> off
    | OpLabel _ ->
        (* When outputing instructions, labels must have been resolved. *)
        assert false
  in
  function
    | AAStore ->
        out8 output 0x53
    | ALoad_0 ->
        out8 output 0x2b
    | ANewArray length ->
        out8 output 0xbd;
        out16 output length
    | Dup ->
        out8 output 0x59
    | GetStatic field_idx ->
        out8 output 0xb2;
        out16 output field_idx
    | Goto branch_off ->
        out8 output 0xa7;
        out16 output (get_offset branch_off)
    | IAdd ->
        out8 output 0x60
    | IConst_0 ->
        out8 output 0x03
    | IConst_1 ->
        out8 output 0x04
    | IDiv ->
        out8 output 0x6c

    | If_ICmpLe branch_off ->
        out8 output 0xa4; out16 output (get_offset branch_off)
    | If_ICmpLt branch_off ->
        out8 output 0xa1; out16 output (get_offset branch_off)
    | If_ICmpGe branch_off ->
        out8 output 0xa2; out16 output (get_offset branch_off)
    | If_ICmpGt branch_off ->
        out8 output 0xa3; out16 output (get_offset branch_off)
    | If_ICmpEq branch_off ->
        out8 output 0x9f; out16 output (get_offset branch_off)
    | IfEq branch_off ->
        out8 output 0x99;
        out16 output (get_offset branch_off)
    | IfNe branch_off ->
        out8 output 0x9a;
        out16 output (get_offset branch_off)

    | ILoad local_idx ->
        out8 output 0x15;
        out8 output local_idx
    | ILoad_0 ->
        out8 output 0x1a
    | IMul ->
        out8 output 0x68
    | INeg ->
        out8 output 0x74
    | InvokeSpecial method_idx ->
        out8 output 0xb7;
        out16 output method_idx
    | InvokeStatic method_idx ->
        out8 output 0xb8;
        out16 output method_idx
    | InvokeVirtual method_idx ->
        out8 output 0xb6;
        out16 output method_idx
    | IReturn ->
        out8 output 0xac
    | IStore local_idx ->
        out8 output 0x36;
        out8 output local_idx
    | ISub ->
        out8 output 0x64
    | Ldc cnst_idx ->
        out8 output 0x12;
        out8 output cnst_idx
    | Pop ->
        out8 output 0x57
    | Return ->
        out8 output 0xb1


(* First output the length of the given list as a 16-bits value, then output
 * each list item using the given output and item output callback. *)
let output_length_prefixed_array output item_output item_list =
  (
    out16 output (List.length item_list);
    List.iter (item_output output) item_list
  )

(*
 * Utilities to output major data types to the class file.
 *)

let output_constant output = function
  | PClass name_idx ->
      out8 output 0x07;
      out16 output name_idx
  | PFieldRef (class_idx, name_and_type_idx) ->
      out8 output 0x09;
      out16 output class_idx;
      out16 output name_and_type_idx
  | PMethodRef (class_idx, name_and_type_idx) ->
      out8 output 0x0a;
      out16 output class_idx;
      out16 output name_and_type_idx
  | PInterfaceMethodRef (class_idx, name_and_type_idx) ->
      out8 output 0x0b;
      out16 output class_idx;
      out16 output name_and_type_idx
  | PStringRef string_idx ->
      out8 output 0x08;
      out16 output string_idx
  | PInteger i ->
      out8 output 0x03;
      out32 output i
  | PFloat f ->
      out8 output 0x04;
      (* TODO: do it! OCaml has no support for IEEE 754 single precision binary
       * output. *)
      failwith "Unsupported constant type: float"
  | PLong l ->
      out8 output 0x05;
      out64 output l
  | PDouble d ->
      out8 output 0x06;
      (* TODO: do it! OCaml has no support for IEEE 754 double precision binary
       * output. *)
      failwith "Unsupported constant type: double"
  | PNameAndType (name_idx, descr_idx) ->
      out8 output 0x0c;
      out16 output name_idx;
      out16 output descr_idx
  | PString str ->
      out8 output 0x01;
      (* TODO: output valid "Java-UTF8" strings (for null and > 127
       * characters) *)
      out16 output (String.length str);
      out_string output str


(* Output a class/field/method attribute into some binary output channel *)
let rec output_attribute output attr =
  out16 output attr.attr_name_idx;
  (* In order to output the byte length of this attribute block, first output it
   * to a buffer, then output the length, and finally output the buffer. *)
  let buf = Buffer.create 100 in
  (
    let output = Utils.stream_from_buffer buf in
    match attr.attr_info with
      | AttrCode code ->
          out16 output code.co_max_stack;
          out16 output code.co_max_locals;
          out32 output (Int32.of_int code.co_code_length);
          List.iter (output_instruction output) code.co_code;
          (* TODO: complete (if needed?) with exception tabe *)
          out16 output 0;
          output_length_prefixed_array output output_attribute code.co_attr
  );
  out32 output (Int32.of_int (Buffer.length buf));
  Utils.transfer_buffer output buf


let output_interface output interface =
  () (* TODO *)


(* Output a member (field or method) into some binary output channel *)
let output_member output member =
  out16 output (access_to_short member.m_access);
  out16 output member.m_name_idx;
  out16 output member.m_descr_idx;
  output_length_prefixed_array output output_attribute member.m_attributes


(* Output a classfile into some binary output channel. *)
let output_classfile classfile output =
  (* Output JVM class files magic number. *)
  List.iter (out8 output) [0xca; 0xfe; 0xba; 0xbe];

  (* Output minor version number and major version number. *)
  let major, minor = classfile.version in
  (
    out16 output minor;
    out16 output (major_to_short major)
  );

  (* Output constant pool entries as a length-prefixed array. *)
  out16 output classfile.const_pool#length;
  classfile.const_pool#iter (output_constant output);

  (* Output access flags (as a bitmask) *)
  out16 output (access_to_short classfile.class_access);

  (* Output the index of exported class and its super class in the constant
   * pool. *)
  out16 output classfile.this_class;
  out16 output classfile.super_class;

  (* Output the interface, field and attribute method tables as length-prefixed
  * arrays. *)
  output_length_prefixed_array output output_interface classfile.interfaces;
  output_length_prefixed_array output output_member classfile.fields;
  output_length_prefixed_array output output_member classfile.methods;
  output_length_prefixed_array output
    output_attribute classfile.class_attributes


let get_classname source =
  (* First check the source filename extension. *)
  let extension_start =
    try
      String.index source '.'
    with
      | Not_found -> -1
  in
  if extension_start < 1 ||
     (String.sub source
       extension_start
       (String.length source - extension_start)) <> ".bl"
  then
    raise (InvalidFilename (source, "Extension must be '.bl'"));

  (* Then check that it contains only valid characters. *)
  let classname = String.sub source 0 extension_start in
  String.iter (function
    | c when
        ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') ||
        ('0' <= c && c <='9') || c = '_'
      -> ()
    | c -> raise (InvalidFilename (
        source,
        Printf.sprintf "Forbidden character: %d (%c)" (Char.code c) c
      ))
  ) classname;
  (* And finally return it. *)
  classname

let mangle = function
  | "" -> "" (* This case should not happen. *)
  (* TODO: escape Java keywords. *)
  | "main" -> "_main"
  | name when name.[0] = '_' -> Printf.sprintf "_id_%s" name
  | name -> name

module OrderedInt = struct
  type t = int
  let compare = Pervasives.compare
end
module IntMap = Map.Make (OrderedInt)

class code_builder = object (self)
  val mutable labels = IntMap.empty
  val mutable next_label = 0
  val instructions = Queue.create ()
  val mutable pc = 0

  (* Create and return a new unique label. *)
  method create_label =
    let label = next_label in
    next_label <- next_label + 1;
    label

  (* Define the address associated to a label as the current location. *)
  method set_label label =
    labels <- IntMap.add label pc labels


  (* Append an instruction, and update the PC. *)
  method emit instr =
    pc <- pc + (get_instruction_length instr);
    Queue.push instr instructions

  (* Return the instruction list with resolved label references. *)
  method get =
    let rec resolve (pc, accu) instr =
      let resolve_lbl = function
        | OpLabel label ->
            OpOffset (IntMap.find label labels - pc)
        | operand -> operand
      in
      let resolved = match instr with
        | Goto op -> Goto (resolve_lbl op)
        | If_ICmpLe op -> If_ICmpLe (resolve_lbl op)
        | If_ICmpLt op -> If_ICmpLt (resolve_lbl op)
        | If_ICmpGe op -> If_ICmpGe (resolve_lbl op)
        | If_ICmpGt op -> If_ICmpGt (resolve_lbl op)
        | If_ICmpEq op -> If_ICmpEq (resolve_lbl op)
        | IfEq op -> IfEq (resolve_lbl op)
        | IfNe op -> IfNe (resolve_lbl op)
        | _ -> instr
      in
      let next_pc = pc + (get_instruction_length resolved) in
      (next_pc, resolved::accu)
    in
    let _, result = Queue.fold resolve (0, []) instructions in
    List.rev result

  method get_pc = pc
end


(* Compile the given expression emitting instructions through the given code
 * builder and emitting constants to the pool and using the given locals mapping
 * for the parameters and local variables. Return the number of stack slots
 * needed by the evaluation of the given expression. *)
let rec compile_expr classname pool_builder code_builder locals expr =
  let compile_expr = compile_expr classname pool_builder code_builder locals in
  match expr with
    | Ast.Int i ->
        let const_idx = pool_builder#get (CInteger (Int32.of_int i)) in
        code_builder#emit (Ldc const_idx);
        1
    | Ast.Var name ->
        let local_slot = Hashtbl.find locals name in
        code_builder#emit (ILoad local_slot);
        1

    | Ast.BinOp (left, op, right) -> (
        (* Since a lot of operators share common code, first declare some
         * utilities. Each one handles one kind of binary operator and returns
         * the number of used stack slots for the evaluation of the
         * subexpression. *)

        (* This one is used for arithmetic operators between two integers. *)
        let compile_arith operation_opcode =
          let slots_left = compile_expr left in
          let slots_right = compile_expr right in
          code_builder#emit operation_opcode;
          (* When the right operand is computed, the left operand takes one
           * slot. *)
          max slots_left (slots_right + 1)

        (* This one is used for comparison between two integers. *)
        and compile_compare get_cmp_opcode =
          let end_label = code_builder#create_label
          and true_label = code_builder#create_label
          in
          let slots_left = compile_expr left in
          let slots_right = compile_expr right in
          (* Branch if the condition is true. *)
          code_builder#emit (get_cmp_opcode true_label);
          (* Otherwise, push false and go to the end of expression
           * evaluation. *)
          code_builder#emit IConst_0;
          code_builder#emit (Goto (OpLabel end_label));
          (* True branch: push true and we are done! *)
          code_builder#set_label true_label;
          code_builder#emit IConst_1;
          (* The false branch must end here. *)
          code_builder#set_label end_label;
          (* Finally, return the stack needs. When the right operand is
          * computed, the left operand takes one slot. *)
          max slots_left (slots_right + 1)

        (* This one is used for logical operations between two integers. *)
        and compile_logic op =
          (* When we do not fallback (ie. evaluating one operand to false for &&
           * or to true for ||), we go to the "breaking_label". *)
          let breaking_label = code_builder#create_label
          (* Every execution path must end up to the "end_label". *)
          and end_label = code_builder#create_label
          (* This is the instruction that checks if we must break or not (and
           * that do it if needed). *)
          in
          let break_if_needed = match op with
            | "&&" ->
                (* Break if last operand evaluated to false. *)
                IfEq (OpLabel breaking_label)
            | "||" ->
                (* Break if last operand evaluated to true. *)
                IfNe (OpLabel breaking_label)
            | _ ->
                (* Invalid operator: should not happen. *)
                assert false
          and fallback_value, break_value = match op with
            | "&&" -> IConst_1, IConst_0
            | "||" -> IConst_0, IConst_1
            | _ ->
                (* Invalid operator: should not happen. *)
                assert false
          in

          (* First have to evaluate the left operand and we break if needed. *)
          let left_slots = compile_expr left in
          code_builder#emit break_if_needed;
          (* Otherwise, evaluate the right operand. *)
          let right_slots = compile_expr right in
          code_builder#emit break_if_needed;
          (* If we didn't broke, we fallback to the... fallback value! *)
          code_builder#emit fallback_value;
          code_builder#emit (Goto (OpLabel end_label));

          (* Here ends "breaking" evaluation. *)
          code_builder#set_label breaking_label;
          code_builder#emit break_value;
          (* Both branch end up here. *)
          code_builder#set_label end_label;
          max (max left_slots right_slots) 1;
        in
        match op with
          | "+" -> compile_arith IAdd
          | "*" -> compile_arith IMul
          | "-" -> compile_arith ISub
          | "/" -> compile_arith IDiv

          | "<" ->
              compile_compare(function label -> If_ICmpLt (OpLabel label))
          | ">" ->
              compile_compare(function label -> If_ICmpGt (OpLabel label))
          | "<=" ->
              compile_compare(function label -> If_ICmpLe (OpLabel label))
          | ">=" ->
              compile_compare(function label -> If_ICmpGe (OpLabel label))
          | "==" ->
              compile_compare(function label -> If_ICmpEq (OpLabel label))
          | "&&" | "||" as op ->
              compile_logic op
          | _ ->
              (* Invalid operator: should not happen. *)
              assert false
      )

    | Ast.UniOp (op, expr) -> (
        match op with
          | "-" ->
              let slots = compile_expr expr in
              code_builder#emit INeg;
              slots
          | "!" ->
              let slots = compile_expr expr
              and true_label = code_builder#create_label
              and end_label = code_builder#create_label in
              (* If the expression evaluates to 0 (false), branch to "true
               * pushing". *)
              code_builder#emit (IfEq (OpLabel true_label));
              (* Otherwise, fallback to "false pushing". *)
              code_builder#emit IConst_0;
              code_builder#emit (Goto (OpLabel end_label));
              (* Push true on the stack. *)
              code_builder#set_label true_label;
              code_builder#emit IConst_1;
              (* Both branch end up here. *)
              code_builder#set_label end_label;
              max slots 1
          | _ ->
              (* Invalid operator: should not happen. *)
              assert false
      )

    | Ast.Call (name, args) ->
        (* All functions take integers and return an integer. *)
        let func_type = type_descriptor_to_string (TypeMethod (
          (List.map (fun _ -> TypeInt) args), TypeInt
        )) in
        let func_ref = pool_builder#get (CMethodRef (
          classname, mangle name, func_type
        )) in
        (* Emit code to compute arguments. *)
        let slots, _ = List.fold_left
          (
            fun (slots, nargs) arg ->
              (* Once this argument is computed, it takes one more slot. *)
              let arg_slots = compile_expr arg in
              (max (nargs + arg_slots) nargs, nargs + 1)
          )
          (0, 0)
          args
        in
        code_builder#emit (InvokeStatic func_ref);
        slots

(* Compile the given statements block emitting instructions through the given
 * code builder and emitting constants to the pool and using the given locals
 * mapping for the parameters and local variables. Return the number of stack
 * slots needed by the evaluation of inner expressions. *)
let rec compile_block classname pool_builder code_builder locals stmts =
  let compile_block =
    compile_block classname pool_builder code_builder locals in
  let compile_expr = compile_expr classname pool_builder code_builder locals in
  let compile_stmt = function
    | Ast.Assign (name, expr) ->
        let local_slot = Hashtbl.find locals name in
        let slots = compile_expr expr in
        code_builder#emit (IStore local_slot);
        slots
    | Ast.Expr expr ->
        let slots = compile_expr expr in
        code_builder#emit Pop;
        slots
    | Ast.If (cond, then_stmts, else_stmts) ->
        let cond_slots = compile_expr cond
        and else_label = code_builder#create_label
        and end_label = code_builder#create_label
        in
        (* If the condition evaluates to false, branch to the else code
         * block... *)
        code_builder#emit (IfEq (OpLabel else_label));
        (* Otherwise, fallback to the if code block. *)
        let then_slots = compile_block then_stmts in
        (* At the end of the if code block, go to the end of the conditional
         * block. *)
        code_builder#emit (Goto (OpLabel end_label));
        (* Here starts the else code block. *)
        code_builder#set_label else_label;
        let else_slots = compile_block else_stmts in
        (* Finally, both branch must end here. *)
        code_builder#set_label end_label;
        (* Return the maximum stack needs. *)
        max cond_slots (max then_slots else_slots)
    | Ast.While (cond, stmts) ->
        let start_label = code_builder#create_label
        and end_label = code_builder#create_label
        in
        code_builder#set_label start_label;
        let cond_slots = compile_expr cond in
        (* If the condition evaluates to false, branch to the end label. *)
        code_builder#emit (IfEq (OpLabel end_label));
        (* Otherwise, fallback to the nested statements. *)
        let stmts_slots = compile_block stmts in
        (* Then compute the condition for the next iteration. *)
        code_builder#emit (Goto (OpLabel start_label));
        (* Any loop breaking ends up here. *)
        code_builder#set_label end_label;
        (* Return the maximum stack needs. *)
        max cond_slots stmts_slots
    | Ast.Return expr ->
        let slots = compile_expr expr in
        code_builder#emit IReturn;
        slots
  in
  List.fold_left
    (
      fun slots stmt ->
        let slots_next = compile_stmt stmt in
        max slots slots_next
    )
    0
    stmts

let compile_function classname pool_builder func =
  let name = mangle func.Ast.fname
  and prototype = type_descriptor_to_string (TypeMethod (
      (* This Blaise function can only take integers as arguments *)
      (List.map (function _ -> TypeInt) func.Ast.fparams),
      (* and it must return an integer. *)
      TypeInt
    ))
  and code_attr_info =
    (* Associate each parameter/variable name to an index in the method's locals
     * table. *)
    let locals =
      let params_count = List.length func.Ast.fparams
      and vars_count = List.length func.Ast.fvars in
      let locals = Hashtbl.create (params_count + vars_count)
      and counter = ref 0 in
      let add_local name =
        Hashtbl.add locals name !counter;
        counter := !counter + 1
      in
      List.iter add_local func.Ast.fparams;
      List.iter add_local func.Ast.fvars;
      locals
    and code_builder = new code_builder
    in

    let code, slots =
      (* Compile the whole function body. *)
      let slots =
        compile_block classname pool_builder code_builder locals func.Ast.fbody
      in
      (* Add a synthetic fallback return statement, just in case. *)
      code_builder#emit IConst_0;
      code_builder#emit IReturn;

      (* Resolve labels. *)
      code_builder#get, (max 1 slots)
    in
    {
      co_max_stack = slots;
      co_max_locals = Hashtbl.length locals;
      (* The length of a code block is what would be the PC of the next
       * instruction. *)
      co_code_length = code_builder#get_pc;
      co_code = code;
      co_attr = [];
    }
  in
  let code_attr =
    {
      attr_name_idx = pool_builder#get (CString "Code");
      attr_info = AttrCode code_attr_info;
    }
  in
  {
    m_access = [Public; Static];
    m_name_idx = pool_builder#get (CString name);
    m_descr_idx = pool_builder#get (CString prototype);
    m_attributes = [code_attr];
  }


let compile source_filename ast output =
  (* There is only one constants pool in a class file: this pool builder will
   * be used for the whole compilation module. *)
  let pool_builder = new constant_pool_builder
  and classname = get_classname source_filename
  in

  (* Get some useful constants: *)
  (* -  references to the main class and its superclass; *)
  let object_idx = pool_builder#get (CClass "java/lang/Object")
  and this_idx = pool_builder#get (CClass classname)

  (* -  reference to the "Code" string (used by Code attributes) *)
  and code_str = pool_builder#get (CString "Code")

  (* Define some useful types: *)
  and integer_type = TypeClassInstance "java/lang/Integer"
  and object_type = TypeClassInstance "java/lang/Object"
  and string_type = TypeClassInstance "java/lang/String"
  in

  (* Define the method_info structure for the "write" method (which is a builtin
   * in the Blaise language). *)
  let write_meth =
    let prototype = type_descriptor_to_string (
      (* "write" takes one integer and returns an integer. *)
      TypeMethod ([TypeInt], TypeInt))
    and code_attr_info =
      {
        co_max_stack = 6;
        co_max_locals = 1;
        co_code_length = 22;
        co_code = (
          (* Get the type and a reference to "System.out". *)
          let printstream_type = (
            TypeClassInstance "java/io/PrintStream") in
          let sys_out =
            pool_builder#get (CFieldRef (
              "java/lang/System", "out",
              type_descriptor_to_string printstream_type))

          (* Get the type and a reference to "System.out.format". *)
          and out_format =
            let method_type = type_descriptor_to_string (TypeMethod (
              [
                TypeClassInstance "java/lang/String";
                TypeArray (object_type)
              ], printstream_type))
            in
            pool_builder#get (CMethodRef (
              "java/io/PrintStream", "format", method_type))

          (* Get a reference to "Integer.valueOf" *)
          and integer_valueof =
            let method_type = type_descriptor_to_string (TypeMethod (
              [TypeInt], integer_type))
            in
            pool_builder#get (CMethodRef (
              "java/lang/Integer", "valueOf", method_type))
          in
          [
            GetStatic sys_out; (* Get "System.out". *)
            (* Load the format string. *)
            Ldc (pool_builder#get (CStringRef "%d\n"));

            (* Create "new Object[1]" and dup it *)
            IConst_1;
            ANewArray object_idx;

            (* Put the integer argument into the array. *)
            Dup;
            IConst_0;
            (* But first convert it to the boxed "Integer" type. *)
            ILoad_0;
            InvokeStatic integer_valueof;
            AAStore;

            (* Call "System.out.format" and give up its result. *)
            InvokeVirtual out_format;
            Pop;

            (* Return 0. *)
            IConst_0;
            IReturn;
          ]
        );
        co_attr = [];
      }
    in
    let code_attr =
      {
        attr_name_idx = code_str;
        attr_info = AttrCode code_attr_info;
      }
    in
    {
      m_access = [Public; Static];
      m_name_idx = pool_builder#get (CString "write");
      m_descr_idx = pool_builder#get (CString prototype);
      m_attributes = [code_attr];
    }
  in

  (* Define the method_info structure for the "main" method. This is a
   * trampoline for the Blaise main function. *)
  let main_meth =
    let prototype = type_descriptor_to_string (
      (* "main" takes an array of strings (argv) and returns nothing. *)
      TypeMethod (
        [TypeArray (string_type)],
        TypeVoid
      ))
    in
    let code_attr_info =
      {
        co_max_stack = 1;
        co_max_locals = 1;
        co_code_length = 5;
        co_code = (
          let main_type = type_descriptor_to_string (TypeMethod ([], TypeInt))
          in
          let main_ref = pool_builder#get (CMethodRef (
            classname, mangle "main", main_type))
          in [
            InvokeStatic main_ref;
            Pop;
            Return
          ]
        );
        co_attr = [];
      }
    in
    let code_attr =
      {
        attr_name_idx = code_str;
        attr_info = AttrCode code_attr_info;
      }
    in
    {
      m_access = [Public; Static];
      m_name_idx = pool_builder#get (CString "main");
      m_descr_idx = pool_builder#get (CString prototype);
      m_attributes = [code_attr];
    }
  in

  let main_as_func =
    {
      Ast.fname = "main";
      Ast.fbody = ast.Ast.main.Ast.mainbody;
      Ast.fparams = [];
      Ast.fvars = ast.Ast.main.Ast.mainvars;
    }
  in
  (* Compile Blaise functions into method_info structures. *)
  let methods = List.fold_left
    (fun accu func -> (compile_function classname pool_builder func)::accu)
    [write_meth; main_meth]
    (main_as_func::ast.Ast.func)
  in

  let classfile =
    {
      version = (J2SE5, 0);
      const_pool = pool_builder;
      class_access = [Public; Super];
      this_class = this_idx;
      super_class = object_idx;
      interfaces = []; (* No superinterface *)
      fields = [];
      methods = methods;
      class_attributes = [];
    }
  in
  output_classfile classfile (Utils.stream_from_channel output)
