(* These exceptions are raised during the semantic analysis if the input AST is
 * not corect. *)
exception AlreadyDefinedFunction of string
exception AlreadyDefinedVariable of (string * string)
exception UndefinedFunction of (string * string)
exception UndefinedVariable of (string * string)
exception UndefinedVariable of (string * string)
exception TypeMismatch of (string * int * int * string)


module StrMap = Map.Make (String)
module StrSet = Set.Make (String)

type fn_type = int
type fn_types = fn_type StrMap.t


(* Pervasives: builtin functions *)
let pervasives = List.fold_left
  (
    fun accu (fn_name, fn_type) ->
      StrMap.add fn_name fn_type accu
  )
  StrMap.empty
  [
    ("write", 1)
  ]


(* Extract functions prototypes from their declarations. *)
let get_function_types fn_decls =

  let fold_types fn_types fn_decl =
    let fn_name = fn_decl.Ast.fname in

    if StrMap.mem fn_name fn_types then
      raise (AlreadyDefinedFunction fn_name);
    let arity = List.length fn_decl.Ast.fparams in
    StrMap.add fn_name arity fn_types
  in

    List.fold_left fold_types pervasives fn_decls


(* Type check the whole source file given functions prototype. *)
let type_check fn_types ast =

  (* Type check a function's body given its attributes (name, parameters,
   * etc.). *)
  let type_fn_body fn_name params vars stmts =

    (* Make up the set of defined variables, check for doubles and then type
     * check statements. *)
    let fold_var accu var =
        if StrSet.mem var accu then
          raise (AlreadyDefinedVariable(var, fn_name));
        StrSet.add var accu
    in
    let params_set = List.fold_left fold_var StrSet.empty params in
    let vars_set = List.fold_left fold_var params_set vars in

    let rec type_stmt = function
      | Ast.Assign (name, expr) ->
          if StrSet.mem name vars_set then
            type_expr expr
          else
            raise (UndefinedVariable (name, fn_name))
      | Ast.Expr expr ->
          type_expr expr
      | Ast.If (cond, then_stmts, else_stmts) ->
          type_expr cond;
          type_stmts then_stmts;
          type_stmts else_stmts
      | Ast.While (cond, stmts) ->
          type_expr cond;
          type_stmts stmts
      | Ast.Return expr ->
          type_expr expr

    and type_expr = function
      | Ast.Int i -> ()
      | Ast.Var name ->
          if not (StrSet.mem name vars_set) then
            raise (UndefinedVariable (name, fn_name))
      | Ast.BinOp (left, op, right) ->
          type_expr left;
          type_expr right
      | Ast.UniOp (op, expr) ->
          type_expr expr
      | Ast.Call (name, args) ->
          (
            try
              let fn_type = StrMap.find name fn_types
              and args_count = List.length args
              in
                (* Check that the correct number of arguments is given... *)
                if args_count <> fn_type then
                  raise (TypeMismatch (name, args_count, fn_type, fn_name));

                (* ... and type check arguments themselves. *)
                List.iter type_expr args

            with
                Not_found -> raise (UndefinedFunction (name, fn_name))
          )

    and type_stmts stmts = List.iter type_stmt stmts
    in
    type_stmts stmts
  in

  (* Type check every function's body... *)
  List.iter
    (function fn_decl ->
        type_fn_body
          fn_decl.Ast.fname
          fn_decl.Ast.fparams fn_decl.Ast.fvars
          fn_decl.Ast.fbody
    )
    ast.Ast.func;

  (* ... and main's body. *)
  let entry = ast.Ast.main in
    type_fn_body "main" [] entry.Ast.mainvars entry.Ast.mainbody


(* Check that the given AST is semantically correct (raise an exception if not)
 * and return a mapping function name -> function type. *)
let analyse ast =

  (* First pass: make the list of declared functions and register their
   * prototypes (only get their arity) *)
  let fn_types = get_function_types ast.Ast.func in

  (* Second pass: type checking and binding: browse every function + main and
   * check that names are correctly used (no undefined variable nor function,
   * and functions arity respected). *)
  type_check fn_types ast;

  fn_types
