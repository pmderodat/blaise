(* These exceptions are raised when trouble occurs during arguments parsing and
 * handling. *)
exception InvalidArguments
exception CannotOpenInput of (string * string)
exception CannotOpenOutput of (string * string)

type operation =
  (* Translate the Blaise source to an OCaml one. *)
  | Ocaml of (in_channel * out_channel)
  (* Compile the Blaise source to JVM bytecode. *)
  | Bytecode of (string * in_channel * out_channel)
  (* Evaluate or compile the Blaise source using LLVM. *)
  | Evaluate of in_channel
  | Compile of (in_channel * out_channel)

(* Parse an array of string (arguments) and return an operation. *)
val parse : string array -> operation
