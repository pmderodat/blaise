(* This module translates the Blaise AST to OCaml code. *)

(* Translation is quite straightforward: most of the Blaise AST nodes have
 * direct OCaml equivalent ones (binary operators, while loops, ...) except for
 * one statement: the return construct.
 *
 * OCaml has no such language builtin to early return from a function (except
 * using the exceptions mechanism, but it's ugly and too easy :-) )
 *
 * Thus, translation is done in two steps: first rewrite Blaise statements to
 * include special blocks (ReturnBlock) that check if the function should return
 * when needed and so that return statements always are at the end of control
 * flow instructions (like if or while loops).
 *)

(* Statements are translated from the AST type to this one before OCaml
 * output. *)
type ocaml_stmt =
  | Assign of string * Ast.expr
  | Expr of Ast.expr
  | If of Ast.expr * ocaml_stmt list * ocaml_stmt list
  | While of Ast.expr * ocaml_stmt list * bool
  | Return of Ast.expr
  | ReturnBlock of ocaml_stmt list * ocaml_stmt list

type ocaml_type = Int | Bool

(* retvar is the name of the variable used in OCaml to detect that a "return
 * early" behavior is needed. *)
let retvar = "_retvar"

let mangle name =
  if name = "" then
    ""
  else match name, name.[0] with
    (* TODO: mangle OCaml keywords *)
    | _, '_' -> Printf.sprintf "_var%s" name
    | name, _ -> name

(* Rewrite statement blocks in order to keep return statements at the end of
 * execution paths. This is needed because in the Blaise langage, the return
 * statement can "return early" and there is no equivalent in OCaml. *)
let rewrite_return fn_body =

  (* Use a queue as "accu" to fold statements in order to keep statements
   * order. *)
  let rec rewrite_block accu previous_can_return = function
    | [] ->
        (accu, previous_can_return)

    | (Ast.Return expr)::_ ->
        ((Return expr)::accu, true)

    | (Ast.If (cond, then_stmts, else_stmts))::stmts ->
        (* First process recursively branch blocks. *)
        let then_queue, then_can_return =
          rewrite_block [] false then_stmts
        and else_queue, else_can_return =
          rewrite_block [] false else_stmts
        in
        (* Then build the resulting "if" statement. *)
        let if_block =
          If (cond, List.rev then_queue, List.rev else_queue)
        in

        (* Depending on branch blocks' use of the "return" statement, wrap with
         * a ReturnBlock. *)
        if then_can_return || else_can_return then
          let trailing_block, _ = rewrite_block [] false stmts
          in
            (
              (ReturnBlock ([if_block], List.rev trailing_block))::accu,
              true
            )
        else
          rewrite_block (if_block::accu) previous_can_return stmts

    | (Ast.While (cond, nested_stmts))::stmts ->
        (* First process recursively the nested block. *)
        let stmts_queue, can_return =
          rewrite_block [] false nested_stmts
        in
        let while_block = While (cond, List.rev stmts_queue, can_return) in

        (* Depending on nested block's use of the "return" statement, wrap with
         * a ReturnBlock. *)
        if can_return then
          let trailing_block, _ = rewrite_block [] false stmts
          in
          (
            (ReturnBlock ([while_block], List.rev trailing_block))::accu,
            true
          )
        else
          rewrite_block (while_block::accu) previous_can_return stmts

    | (Ast.Assign (name, expr))::stmts ->
        rewrite_block ((Assign (name, expr))::accu) previous_can_return stmts

    | (Ast.Expr expr)::stmts ->
        rewrite_block ((Expr expr)::accu) previous_can_return stmts
  in

  (* Rewrite the given block and finally return a list of statements. *)
  let stmts_queue, can_return = rewrite_block [] false fn_body in
  List.rev stmts_queue


let translate ast output =

  let fmt = Format.formatter_of_out_channel output in
  let emit fmt = Format.fprintf fmt in

  let rec translate_func func =
    (* Function declaration: name and arguments *)
    emit fmt "%s" func.Ast.fname;
    List.iter
      (function param -> emit fmt "@;<1 2>%s" param)
      func.Ast.fparams;
    emit fmt "@;=@]@;";

    (* Function prologue: replace parameters with references. *)
    (
      match func.Ast.fparams with
        | [] -> ()
        | param::params ->
            emit fmt "@[<h>let %s = ref %s@]@;" param param;
            List.iter
              (function param ->
                emit fmt "@[<h>and %s = ref %s@]@;" param param)
              params;
            emit fmt "@[<h>in@]@;"
    );
    emit fmt "@;";

    translate_proc func.Ast.fvars (rewrite_return func.Ast.fbody);
    emit fmt "@]"

  and translate_proc vars stmts =
    (* Procedure prologue: declare the return variable and variables. *)
    (
      match vars with
        | [] -> ()
        | var::vars ->
            emit fmt "@[<h>let %s = ref 0@]@;" var;
            List.iter
              (function var -> emit fmt "@[<h>and %s = ref 0@]@;" var)
              vars;
    );
    if vars <> [] then
      emit fmt "@[<h>in@]@;";

    emit fmt "@[<h>let %s = ref None in@]@;" retvar;

    (* Output the code block itself. *)
    translate_block stmts;

    (* Procedure epilogue: use the return variable. *)
    emit fmt ";@;@[<v 2>";
    emit fmt "@[<h>match !%s with@]@;" retvar;
    emit fmt "@[<h>| None -> 0@]@;";
    emit fmt "@[<h>| Some value -> value@]";
    emit fmt "@]";

  and translate_block stmts =
    emit fmt "@[<v 2>(";
    let rec join = function
      | [] -> ()
      | stmt::[] ->
          emit fmt "@;";
          translate_stmt stmt;
      | stmt::stmts ->
          emit fmt "@;";
          translate_stmt stmt;
          emit fmt ";";
          join stmts
    in
    join stmts;
    emit fmt "@]@;)"

  and translate_stmt stmt =
    match stmt with
      | Assign (name, expr) ->
          emit fmt "@[<hov 2>%s :=@;" name;
          translate_expr expr;
          emit fmt "@]";

      | Expr expr ->
          emit fmt "@[<hov 2>ignore@;";
          translate_expr expr;
          emit fmt "@]"

      | If (cond, then_stmts, else_stmts) ->
          emit fmt "@[<hov 2>if@;";
          translate_expr_to_type Bool cond;
          emit fmt "@;then@]@;";
          translate_block then_stmts;
          if else_stmts <> [] then
            (
              emit fmt "@;else@;";
              translate_block else_stmts;
            )

      | While (cond, stmts, can_return) ->
          emit fmt "@[<hov 2>while@;";
          if can_return then
            emit fmt "(!%s = None) &&@;" retvar;
          translate_expr_to_type Bool cond;
          emit fmt "@;do@]@;";
          translate_block stmts;
          emit fmt "@;done";

      | Return expr ->
          emit fmt "@[<hov 2>%s := Some@;" retvar;
          translate_expr expr;
          emit fmt "@]"

      | ReturnBlock (entry, trailing) ->
          emit fmt "@[<v 2>(@;";
          translate_block entry;
          emit fmt ";@;";
          emit fmt "@[<v 2>";
          emit fmt "@[<h>match !%s with@]@;" retvar;
          emit fmt "@[<v 2>| None ->@;";
          translate_block trailing;
          emit fmt "@]@;";
          emit fmt "@[<h>| _ -> ()@]";
          emit fmt "@]@]@;)"

  (* By default, an expression is translated to an integer expression. *)
  and translate_expr expr = translate_expr_to_type Int expr

  (* Emit convertion code if necessary to adapt known-type expression to fit
  * outer expression type. *)
  and translate_typed_expr outer_type expr_type emit_expr =
    match (outer_type, expr_type) with
      | (Bool, Int) ->
          (* Convert integer to boolean. *)
          emit fmt "((";
          emit_expr ();
          emit fmt ") <> 0)@;"

      | (Int, Bool) ->
          (* Convert boolean to integer. *)
          emit fmt "@[<v 2>(";
          emit fmt "@[<2>@,match@;";
          emit_expr ();
          emit fmt "@;with@]@;";
          emit fmt "@[<h>| false -> 0@]@;";
          emit fmt "@[<h>| true -> 1@]@;";
          emit fmt "@]@;)"

      | _ ->
          (* Let the expression as-is. *)
          emit_expr ()

  and translate_expr_to_type outer_type = function
    | Ast.Int i ->
        translate_typed_expr outer_type Int
          (fun () -> emit fmt "%d@," i)

    | Ast.Var name ->
        translate_typed_expr outer_type Int
          (fun () -> emit fmt "!%s@," (mangle name))

    | Ast.BinOp (left, op, right) ->
        let return_type = match op with
          | "<" | ">" | "<=" | ">=" | "==" | "&&" | "||" -> Bool
          | _ -> Int
        and operands_type = match op with
          | "&&" | "||" -> Bool
          | _ -> Int
        (* Some operators have a different syntax in OCaml. *)
        and ocaml_op = match op with
          | "==" -> "="
          | op -> op
        in

        let emit_expr () =
          emit fmt "@[<2>(@,";
          translate_expr_to_type operands_type left;
          emit fmt "@;%s@;" ocaml_op;
          translate_expr_to_type operands_type right;
          emit fmt "@,)@]@,"
        in
        translate_typed_expr outer_type return_type emit_expr

    | Ast.UniOp (op, expr) ->
        let emit_op, return_type, operand_type = match op with
            | "!" -> ((fun () -> emit fmt "not@;"), Bool, Bool)
            | op -> ((fun () -> emit fmt "%s" op), Int, Int)
        in
        let emit_sub_expr () =
          emit_op ();
          translate_expr_to_type operand_type expr
        in
        translate_typed_expr outer_type return_type emit_sub_expr

    | Ast.Call (name, args) ->
        let emit_expr () =
          let name, returns_int =
            if name = "write" then
              ("Printf.printf \"%d\\n\"", false)
            else
              (name, true)
          in
          emit fmt "@[<2>(@,%s@;" name;
          let rec translate_args = function
            | [] -> ()
            | first::[] -> translate_expr first;
            | first::args ->
                translate_expr first;
                emit fmt "@;";
                translate_args args
          in
          translate_args args;
          if not returns_int then
            emit fmt ";@;0";
          emit fmt "@,@])"
        in
        translate_typed_expr outer_type Int emit_expr
  in

  emit fmt "@[<v>";

  (* Translate all functions *)
  let rec join = function
    | [] -> ()
    | func::[] ->
        translate_func func;
        emit fmt "@]@;@\n"
    | func::funcs ->
        translate_func func;
        emit fmt "@]@;@\n";
        emit fmt "@[<v 2>";
        emit fmt "@[<b 2>and "
  in
  if ast.Ast.func <> [] then
    (
      emit fmt "@[<v 2>";
      emit fmt "@[<b 2>let rec ";
      join ast.Ast.func;
    );

  (* Translate the entry statements *)
  let entry = ast.Ast.main in
  emit fmt "@[<v 2>let _ =@;";
  translate_proc entry.Ast.mainvars (rewrite_return entry.Ast.mainbody);
  emit fmt "@]";

  emit fmt "@]@.";
