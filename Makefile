PROGRAM = blaise

OCAMLBUILDFLAGS = \
	-I llvm \
	-lib unix \
	-lib llvm \
	-lib llvm_analysis \
	-lib llvm_bitwriter \
	-lib llvm_executionengine \
	-lib llvm_scalar_opts \
	-lib llvm_target \
	-lflag -cc -lflag g++ \
	-lflags -ccopt -lflag "$(llvm-config --ldflags)" \
	-lflags -ccopt -lflag "$(llvm-config --libs)"

all: $(PROGRAM).native
.PHONY: clean distclean $(PROGRAM).native $(PROGRAM).byte $(PROGRAM).debug

clean:
distclean: clean
	rm -rf _build $(PROGRAM).native $(PROGRAM).byte $(PROGRAM).debug

$(PROGRAM).native:
	ocamlbuild $(OCAMLBUILDFLAGS) $(PROGRAM).native

$(PROGRAM).byte:
	ocamlbuild $(OCAMLBUILDFLAGS) $(PROGRAM).byte

$(PROGRAM).debug:
	ocamlbuild -tag debug -lflag -g $(OCAMLBUILDFLAGS) $(PROGRAM).byte
	ln -fs $(PROGRAM).byte $(PROGRAM).debug
