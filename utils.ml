(* Abstract the stream concept from the file/buffer implementations for JVM binary
* outputs (ie. big endian byte order). *)

type out_stream =
  | File of out_channel
  | Buffer of Buffer.t

let stream_from_channel channel = File channel
let stream_from_buffer buf = Buffer buf

let short_mask = Int32.of_int 0xffff

let out8 output byte = match output with
  | File chan -> output_byte chan byte
  | Buffer buf -> Buffer.add_char buf (Char.chr byte)
let out16 output short =
  (* First convert signed shorts to unsigned ones. *)
  let short = short land 0xffff in
  out8 output (short lsr 8);
  out8 output (short land 0xff)
let out32 output int_ =
  let out16 short = out16 output (Int32.to_int short) in
  out16 (Int32.shift_right_logical int_ 16);
  out16 (Int32.logand int_ short_mask)
let out64 output long =
  out32 output (Int64.to_int32 (Int64.shift_right_logical long 32));
  out32 output (Int64.to_int32 long)

let out_string output str = match output with
  | File chan -> output_string chan str
  | Buffer buf -> Buffer.add_string buf str

let transfer_buffer output in_buf =
  let transferer = match output with
    | File chan -> Buffer.output_buffer chan
    | Buffer out_buf -> Buffer.add_buffer out_buf
  in
  transferer in_buf
