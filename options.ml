(* These exceptions are raised when trouble occurs during arguments parsing and
 * handling. *)
exception InvalidArguments
exception CannotOpenInput of (string * string)
exception CannotOpenOutput of (string * string)

type operation =
  (* Translate the Blaise source to an OCaml one. *)
  | Ocaml of (in_channel * out_channel)
  (* Compile the Blaise source to JVM bytecode. *)
  | Bytecode of (string * in_channel * out_channel)
  (* Evaluate or compile the Blaise source using LLVM. *)
  | Evaluate of in_channel
  | Compile of (in_channel * out_channel)

(* Parse an array of string (arguments) and return an operation. *)
let parse args =

  let open_files source output output_is_bin =
    (* Depending on the cases, the output can be a text file or a binary one. *)
    let open_output =
      if output_is_bin then
        open_out_bin
      else
        open_out
    in
    let f_source = (
      try
        open_in source
      with
          Sys_error msg -> raise (CannotOpenInput (source, msg))
    )
    and f_output = (
      try
        open_output output
      with
          Sys_error msg -> raise (CannotOpenOutput (output, msg))
    )
    in (f_source, f_output)
  in

  let rec parse = function
    | "-ocaml"::source::output::[] ->
        Ocaml (open_files source output false)
    | "-byte"::source::output::[] ->
        let source_chan, output_chan = open_files source output true in
        Bytecode (source, source_chan, output_chan)
    | "-eval"::source::[] ->
        Evaluate (
          try open_in source
          with Sys_error msg -> raise (CannotOpenInput (source, msg))
        )
    | "-comp"::source::output::[] ->
        Compile (open_files source output true)
    | _ -> raise InvalidArguments
  in

  match Array.to_list args with
    | [] -> assert false (* There should be at least the program name in args *)
    | argv0::other_args -> parse other_args
