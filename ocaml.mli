(* This module translates the Blaise AST to OCaml code. *)

val translate : Ast.prg -> out_channel -> unit
