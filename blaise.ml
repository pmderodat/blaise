exception GenericError of string

let op_err msg =
  Printf.eprintf "Error: %s\n" msg;
  Printf.eprintf "Aborting\n";
  1


let parse_and_check source =
  let ast =
    (* TODO: handle lexing/parsing/opening exceptions. *)
    Parser.prg Lexer.token (Lexing.from_channel source)
  in
  close_in source;
  (
    try
      ignore (Semantic.analyse ast)
    with
      | Semantic.AlreadyDefinedFunction fname ->
          raise (GenericError
               (Printf.sprintf "function %s is defined twice" fname)
          )
      | Semantic.AlreadyDefinedVariable (var, func) ->
          raise (GenericError
            (Printf.sprintf
              "variable %s in function %s is defined twice" var func
            )
          )
      | Semantic.UndefinedFunction (fname, func) ->
          raise (GenericError
            (Printf.sprintf
              "function %s is used in function %s but is never defined"
              fname func
            )
          )
      | Semantic.UndefinedVariable (var, func) ->
          raise (GenericError
            (Printf.sprintf
              "variable %s in function %s is used but never defined"
              var func
            )
          )
      | Semantic.TypeMismatch (fname, args_count, fn_type, func) ->
          raise (GenericError
            (Printf.sprintf
              "function %s is called with %d arguments in function %s, but it expects %d ones"
              fname args_count func fn_type
            )
          )
  );
  ast


let main () =
  (* First, parse arguments to know what to do. *)
  let operation =
    try
      Options.parse Sys.argv
    with
      | Options.InvalidArguments ->
          Printf.eprintf "Error: invalid arguments\n";
          Printf.eprintf "Usage: %s [-ocaml | -byte | -eval | -comp] <source> <output>\n"
            Sys.argv.(0);
          exit 1
      | Options.CannotOpenInput(fname, msg) ->
          Printf.eprintf "Error: cannot open input file '%s': %s\n" fname msg;
          exit 1
      | Options.CannotOpenOutput(fname, msg) ->
          Printf.eprintf "Error: cannot open output file '%s': %s\n" fname msg;
          exit 1
  in

  (* Then, dispatch execution flow to the good engine. *)
  match operation with
    | Options.Ocaml(source, output) ->
        (try
            let ast = parse_and_check source in
            Ocaml.translate ast output;
            close_out output;
            0
        with
          | GenericError msg ->
              op_err msg)

    | Options.Bytecode(source_filename, source, output) -> (
        try
            let ast = parse_and_check source in
            Jvm.compile source_filename ast output;
            close_out output;
            0
        with
          | GenericError msg ->
              op_err msg
          | Jvm.InvalidFilename (source, msg) ->
              op_err (Printf.sprintf "Invalid filename '%s': %s" source msg)
      )

    | Options.Evaluate source -> (
        try
            let ast = parse_and_check source in
            Llvm_eval.eval ast
        with
          | GenericError msg ->
              op_err msg
          | Llvm_be.CompilationError msg ->
              op_err (Printf.sprintf "Compiler internal error: %s" msg)
      )

    | Options.Compile(source, output) -> (
        try
            let ast = parse_and_check source in
            Llvm_comp.compile ast output;
            close_out output;
            0
        with
          | GenericError msg ->
              op_err msg
          | Llvm_be.CompilationError msg ->
              op_err (Printf.sprintf "Compiler internal error: %s" msg)
          | Llvm_comp.UnknownError ->
              op_err "Error when write to output (I don't know why)";
      )

let _ = exit (main ())
