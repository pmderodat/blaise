(* This module provides utilities to encapsulate compilation state (mostly LLVM
 * values) into a single object: the "state". *)


(* Only one type is allowed in Blaise: integers. Integers size is determined by
 * the architecture. *)
type int_size =
  | Int32
  | Int64

(* Instances of this class gather values needed to compile one compilation unit
 * (a Blaise source file). *)
class state :
  int_size ->
  object
    method get_context : Llvm.llcontext
    method get_module : Llvm.llmodule
    method get_pass_manager : [ `Function ] Llvm.PassManager.t
    method get_builder : Llvm.llbuilder

    method get_int_type : Llvm.lltype

    method get_builtin : string -> Llvm.llvalue
    method register_builtin : string -> Llvm.llvalue -> unit
  end
