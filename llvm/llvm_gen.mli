(* This module takes care of translating the Blaise AST to LLVM IR. *)


(* Declare a function and return it. *)
val gen_func_decl :
  <
    get_module : Llvm.llmodule;
    get_int_type : Llvm.lltype;
    ..
  > ->
  Ast.funcdecl ->
  Llvm.llvalue

(* Generate LLVM code for the given function. *)
val gen_func_body :
  <
    get_context : Llvm.llcontext;
    get_module : Llvm.llmodule;
    get_pass_manager : [ `Function ] Llvm.PassManager.t;
    get_builder : Llvm.llbuilder;

    get_int_type : Llvm.lltype;

    get_builtin : string -> Llvm.llvalue;
    register_builtin : string -> Llvm.llvalue -> unit;

    ..
  > ->
  Ast.funcdecl ->
  unit
