exception UnknownError


let compile ast output =
  let state = new Llvm_state.state Llvm_state.Int32 in
  let pm = state#get_pass_manager in

  (* Do simple "peephole" optimizations and bit-twiddling optimizations. *)
  Llvm_scalar_opts.add_instruction_combination pm;
  Llvm_scalar_opts.add_reassociation pm;
  (* Eliminate common subexpressions. *)
  Llvm_scalar_opts.add_gvn pm;
  Llvm_scalar_opts.add_cfg_simplification pm;
  ignore (Llvm.PassManager.initialize pm);

  (* Compile the Blaise AST down to LLVM IR. *)
  Llvm_be.compile ast state;

  if not (
    Llvm_bitwriter.output_bitcode ~unbuffered:false output state#get_module
  ) then
    raise UnknownError
