(* Only one type is allowed in Blaise: integers. Integers size is determined by
 * the architecture. *)
type int_size =
  | Int32
  | Int64

(* Instances of this class gather values needed to compile one compilation unit
 * (a Blaise source file). *)
class state int_size =
  let context_ = Llvm.global_context () in
  let m = Llvm.create_module context_ "main" in
  object (self)
    val context = context_
    val module_ = m
    val pm = Llvm.PassManager.create_function m
    val builder = Llvm.builder context_

    method get_context = context
    method get_module = module_
    method get_pass_manager = pm
    method get_builder = builder

    val int_type = match int_size with
      | Int32 -> Llvm.i32_type context_
      | Int64 -> Llvm.i64_type context_
    method get_int_type = int_type

    val builtins:(string, Llvm.llvalue) Hashtbl.t = Hashtbl.create 1
    method get_builtin name = Hashtbl.find builtins name
    method register_builtin name func = Hashtbl.add builtins name func
  end
