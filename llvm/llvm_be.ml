exception CompilationError of string


(* Generate code for builtins and fill the state's hashtable that maps builtin
 * names to their function declaration. *)
let gen_builtins state =
  let int_type = state#get_int_type
  and char_ptr =
    Llvm.pointer_type (Llvm.i8_type state#get_context)
  in

  (* Declare the external printf function and define the write one. *)
  let printf_obj =
    let printf_type =
      Llvm.var_arg_function_type int_type (Array.make 1 char_ptr)
    in
    Llvm.declare_function "printf" printf_type state#get_module

  and write_obj =
    let write_type =
      Llvm.function_type int_type (Array.make 1 int_type)
    in
    Llvm.declare_function "_write" write_type state#get_module

  and fmt_str =
    let str = Llvm.const_stringz state#get_context "%d\n" in
    Llvm.define_global "write_fmt_str" str state#get_module

  and zero = Llvm.const_null int_type
  and builder = state#get_builder
  in

  Llvm.set_linkage Llvm.Linkage.External printf_obj;
  Llvm.set_linkage Llvm.Linkage.Private write_obj;
  Llvm.set_linkage Llvm.Linkage.Private fmt_str;

  (* Create the function's entry basic block. *)
  let bb = Llvm.append_block state#get_context "entry" write_obj in
  Llvm.position_at_end bb builder;
  let fmt_str_arg = Llvm.build_in_bounds_gep fmt_str [|zero; zero|] "" builder
  and int_arg = Llvm.param write_obj 0
  in
  ignore (Llvm.build_call printf_obj [|fmt_str_arg; int_arg|] "" builder);
  ignore (Llvm.build_ret zero builder);

  state#register_builtin "write" write_obj


let compile ast state =
  (* Consider the main block as a usual function. *)
  let main_func =
    {
      Ast.fname = "main";
      Ast.fbody = ast.Ast.main.Ast.mainbody;
      Ast.fparams = [];
      Ast.fvars = ast.Ast.main.Ast.mainvars;
    }
  in
  let funcs = main_func::ast.Ast.func in

  (* First add builtin functions. *)
  gen_builtins state;

  (* Then declare each function. *)
  List.iter (function func -> ignore (Llvm_gen.gen_func_decl state func)) funcs;
  (* And finally generate code for functions bodies. *)
  List.iter (function func -> Llvm_gen.gen_func_body state func) funcs;

  match Llvm_analysis.verify_module state#get_module with
    | Some err -> raise (CompilationError err)
    | None -> ()
