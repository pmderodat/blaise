(* Blaise's LLVM evaluation backend. *)

val eval : Ast.prg -> int
