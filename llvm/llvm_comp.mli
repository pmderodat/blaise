(* Blaise's LLVM compilation backend. *)

(* Raised when bitfile output failed. There is no way to know why. *)
exception UnknownError

val compile : Ast.prg -> out_channel -> unit
