let mangle_local name =
  Printf.sprintf "_var_%s" name

let mangle_func = function
  | name when name.[0] = '_' -> Printf.sprintf "_id_%s" name
  | name -> name


(* Declare a function and return it. *)
let gen_func_decl state func =
  (* First make up the function type (remember, all functions return an
   * integer. *)
  let func_type =
    let args_type =
      Array.make (List.length func.Ast.fparams) state#get_int_type
    in
    Llvm.function_type state#get_int_type args_type
  in
  let func_obj =
    let func_name = mangle_func func.Ast.fname in
    Llvm.declare_function func_name func_type state#get_module
  in
  let func_params = Llvm.params func_obj in

  (* Associate a name to each argument. *)
  List.iteri (
    fun i name ->
      let param = func_params.(i) in
      Llvm.set_value_name name param
  ) func.Ast.fparams;

  (* Finally, return the function itself. *)
  func_obj


(* Generate LLVM code for the given statements. *)
let gen_code_block state vars stmts =

  (* Simple utility that return an llvalue to access the memory location
   * corresponding to the given variable name. *)
  let get_var name =
    Hashtbl.find vars (mangle_local name)
  and context = state#get_context
  and builder = state#get_builder
  and int_type = state#get_int_type
  in

  let zero = Llvm.const_null int_type in
  let one = Llvm.const_int int_type 1 in

  (* Real recursive function that generate LLVM code for statements blocks.
  * Return whether the last statement generated is a termination one (ret, br,
  * ...) *)
  let rec gen_block_helper = function
    | [] -> false
    | (Ast.Assign (name, expr))::tail ->
        let value = gen_expr_code expr in
        ignore (Llvm.build_store value (get_var name) builder);
        gen_block_helper tail

    | (Ast.Expr (expr))::tail ->
        ignore (gen_expr_code expr);
        gen_block_helper tail

    | (Ast.If (cond, then_stmts, else_stmts))::tail ->
        let cond_bb = Llvm.insertion_block builder in
        let func_obj = Llvm.block_parent cond_bb in

        (* First generate the code executed when the condition is met. *)
        let then_first_bb = Llvm.append_block context "then" func_obj in
        Llvm.position_at_end then_first_bb builder;
        let then_terminates = gen_block_helper then_stmts in
        (* Get the basic block that ends the "then" statements block. *)
        let then_last_bb = Llvm.insertion_block builder in

        (* Then generate the code executed otherwise. *)
        let else_first_bb = Llvm.append_block context "else" func_obj in
        Llvm.position_at_end else_first_bb builder;
        let else_terminates = gen_block_helper else_stmts in
        (* Get the basic block that ends the "else" statements block. *)
        let else_last_bb = Llvm.insertion_block builder in

        (* Now we have the two "destination" basic blocks, we can build the
         * conditional branch. *)
        Llvm.position_at_end cond_bb builder;
        let cond_value = gen_expr_code cond in
        let do_branch =
          Llvm.build_icmp Llvm.Icmp.Ne cond_value zero "" builder
        in
        ignore (
          Llvm.build_cond_br
            do_branch
            then_first_bb else_first_bb
            builder
        );

        if not (then_terminates && else_terminates) then
          (
            (* Generate the continuation basic block if needed. *)
            let cont_bb = Llvm.append_block context "" func_obj in

            (* And branch to it at the end of the then/else path. *)
            if not then_terminates then
              (
                Llvm.position_at_end then_last_bb builder;
                ignore (Llvm.build_br cont_bb builder)
              );
            if not else_terminates then
              (
                Llvm.position_at_end else_last_bb builder;
                ignore (Llvm.build_br cont_bb builder)
              );

            (* Next statement must append code to the continuation basic block. *)
            Llvm.position_at_end cont_bb builder;
            gen_block_helper tail
          )
        else
          true

    | (Ast.While (cond, do_stmts))::tail ->
        let previous_bb = Llvm.insertion_block builder in
        let func_obj = Llvm.block_parent previous_bb in

        (* Create a basic block just for the conditional branch. *)
        let condition_bb = Llvm.append_block context "while_cond" func_obj in

        (* Generate the code executed when the condition is met. *)
        let do_first_bb = Llvm.append_block context "do" func_obj in
        Llvm.position_at_end do_first_bb builder;
        let do_terminates = gen_block_helper do_stmts in
        (* Get the basic block that ends the "do" statements block. *)
        let do_last_bb = Llvm.insertion_block builder in

        (* Always add a continuation basic block: even if the while nested
         * statements always return, the while condition could not be met. *)
        let cont_bb = Llvm.append_block context "" func_obj in

        (* Now we have the all basic blocks, we can build all the branches. *)
        (* First branch from previous block to condition basic block. *)
        Llvm.position_at_end previous_bb builder;
        ignore (Llvm.build_br condition_bb builder);

        (* Then branch from the condition basic block to the nested statements
         * or to the continuation block. *)
        Llvm.position_at_end condition_bb builder;
        let cond_value = gen_expr_code cond in
        let do_branch =
          Llvm.build_icmp Llvm.Icmp.Ne cond_value zero "" builder
        in
        ignore (
          Llvm.build_cond_br
            do_branch
            do_first_bb cont_bb
            builder
        );

        (* If the nested statements don't always return, branch to the condition
         * block again at their end. *)
        if not do_terminates then
          (
            Llvm.position_at_end do_last_bb builder;
            ignore (Llvm.build_br condition_bb builder)
          );

        (* Next statement must append code to the continuation basic block. *)
        Llvm.position_at_end cont_bb builder;
        gen_block_helper tail

    | (Ast.Return expr)::tail ->
        let value = gen_expr_code expr in
        ignore (Llvm.build_ret value builder);
        true
        (* There is no need to generate the trailing statements since they are
         * unreachable. *)

  (* Generate LLVM code to computing the given expression and return the
   * corresponding llvalue. *)
  and gen_expr_code = function
    | Ast.Int value ->
        Llvm.const_int int_type value
    | Ast.Var name ->
        Llvm.build_load (get_var name) "" builder
    | Ast.BinOp (left, op, right) ->
        if op <> "&&" && op <> "||" then
          (
            let left = gen_expr_code left
            and right = gen_expr_code right
            in

            (* Generate an arithmetic operation. *)
            let gen_arith_code arith =
              arith left right "" builder
            (* Generate an integer comparison. *)
            and gen_icmp_code icmp =
              let bit = Llvm.build_icmp icmp left right "" builder in
              Llvm.build_zext bit int_type "" builder

            in
            (
              match op with
                | "+" -> gen_arith_code Llvm.build_nsw_add
                | "*" -> gen_arith_code Llvm.build_nsw_mul
                | "-" -> gen_arith_code Llvm.build_nsw_sub
                | "/" -> gen_arith_code Llvm.build_sdiv

                | "<" -> gen_icmp_code Llvm.Icmp.Slt
                | ">" -> gen_icmp_code Llvm.Icmp.Sgt
                | "<=" -> gen_icmp_code Llvm.Icmp.Sle
                | ">=" -> gen_icmp_code Llvm.Icmp.Sge
                | "==" -> gen_icmp_code Llvm.Icmp.Eq

                | _ ->
                    (* Invalid operator: should not happen. *)
                    assert false
            )
          )
        else
          (
            let previous_bb = Llvm.insertion_block builder in
            let func_obj = Llvm.block_parent previous_bb in

            let right_bb = Llvm.append_block context "right" func_obj
            and final_bb = Llvm.append_block context "break" func_obj
            and break_cond, break_value = match op with
              (* If left evaluates to something *equal to* zero, break and
              * return *zero*. *)
              | "&&" -> Llvm.Icmp.Eq, zero
              (* If left evaluates to something *different to* but zero, break
               * and return *one*. *)
              | "||" -> Llvm.Icmp.Ne, one
              | _ ->
                  (* Invalid operator: should not happen. *)
                  assert false

            (* First evaluate the left expression. *)
            and left = gen_expr_code left
            in
            (* If left met the break condition, ... break! *)
            let do_break = Llvm.build_icmp break_cond left zero "" builder in
            ignore (Llvm.build_cond_br do_break final_bb right_bb builder);

            (* If we did not break, evaluate (right expression <> 0) and branch
             * to the final basic block. *)
            Llvm.position_at_end right_bb builder;
            let right = gen_expr_code right in
            let is_true = Llvm.build_icmp Llvm.Icmp.Ne right zero "" builder in
            let result = Llvm.build_zext is_true int_type "" builder in
            ignore (Llvm.build_br final_bb builder);

            (* In the final basic block, just take the value "to return". *)
            Llvm.position_at_end final_bb builder;
            Llvm.build_phi
              [
                (* Either the left value was breaking: take the break value. *)
                (break_value, previous_bb);
                (* Otherwise, take whether the right evaluated to true. *)
                (result, right_bb)
              ]
              "" builder
          )
    | Ast.UniOp (op, expr) -> (
        match op with
          | "!" -> gen_expr_code (Ast.BinOp (expr, "==", Ast.Int 0))
          | "-" ->
              let value = gen_expr_code expr in
              Llvm.build_nsw_sub zero value "" builder
          | _ ->
              (* Invalid operator: should not happen. *)
              assert false
      )
    | Ast.Call (name, args) -> (
        (* If there is no such function, it must be a builtin. *)
        let func_obj =
          match Llvm.lookup_function (mangle_func name) state#get_module with
          | Some func -> func
          | None -> state#get_builtin name
        and args = Array.of_list (List.map gen_expr_code args)
        in
        Llvm.build_call func_obj args "" builder
      )

  in

  if not (gen_block_helper stmts) then
    (* Return zero as a fallback path. *)
    ignore (Llvm.build_ret zero builder)


(* Generate LLVM code for the given function. *)
let gen_func_body state func =

  let func_obj =
    let func_name = mangle_func func.Ast.fname in
    match Llvm.lookup_function func_name state#get_module with
      | Some f -> f
      | None ->
          (* Cannot happen: all functions must have been declared. *)
          assert false
  and builder = state#get_builder
  and int_type = state#get_int_type
  in

  (* Create the function's entry basic block. *)
  let bb = Llvm.append_block state#get_context "entry" func_obj in
  Llvm.position_at_end bb builder;


  (* This table associates mutable variables to their memory location. *)
  let vars = Hashtbl.create (
    (List.length func.Ast.fparams) + (List.length func.Ast.fvars)
  ) in

  (* Declare mutable variables for arguments and local variables. *)
  let create_var name =
    let mangled_name = mangle_local name in
    (* Allocate some memory location. *)
    let var = Llvm.build_alloca int_type mangled_name builder in
    (* Associate the variable name to its memory location. *)
    Hashtbl.add vars mangled_name var;
    (* And return the memory location itself. *)
    var
  in

  (* First create the mutable variables for the arguments. *)
  List.iteri (
    fun i name ->
      (* Create the mutable variable. *)
      let param = Llvm.param func_obj i
      and var = create_var name
      in
      (* And store the value of the argument into the mutable variable. *)
      ignore (Llvm.build_store param var builder)
  ) func.Ast.fparams;

  (* Then do the same for the local variables. *)
  let zero = Llvm.const_null int_type in
  List.iteri (
    fun i name ->
      (* Create the mutable variable. *)
      let var = create_var name in
      (* Initialize it to zero. *)
      ignore (Llvm.build_store zero var builder)
  ) func.Ast.fvars;

  (* Then generated code for the function's body. *)
  gen_code_block state vars func.Ast.fbody;

  (* Validate and optimize the generated code. *)
  if Llvm_analysis.verify_function func_obj then
    ignore (
      Llvm.PassManager.run_function func_obj state#get_pass_manager
    )
