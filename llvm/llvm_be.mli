(* Blaise's LLVM backend. *)

(* Raised when generated LLVM IR is invalid. If it is raised, our compiler is
 * bugged! *)
exception CompilationError of string


val compile :
  Ast.prg ->
  <
    get_context : Llvm.llcontext;
    get_module : Llvm.llmodule;
    get_pass_manager : [ `Function ] Llvm.PassManager.t;
    get_builder : Llvm.llbuilder;

    get_int_type : Llvm.lltype;

    get_builtin : string -> Llvm.llvalue;
    register_builtin : string -> Llvm.llvalue -> unit;
    ..
  > ->
  unit
