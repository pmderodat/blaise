module ExecutionEngine = Llvm_executionengine.ExecutionEngine
module GenericValue = Llvm_executionengine.GenericValue
module PassManager = Llvm.PassManager

let eval ast =
  let state = new Llvm_state.state Llvm_state.Int32 in
  let pm = state#get_pass_manager in

  (* Create if possible a JIT. *)
  let exec_engine =
    ExecutionEngine.create state#get_module
  in

  Llvm_target.DataLayout.add
    (ExecutionEngine.target_data exec_engine)
    pm;
  (* Do simple "peephole" optimizations and bit-twiddling optimizations. *)
  Llvm_scalar_opts.add_instruction_combination pm;
  Llvm_scalar_opts.add_reassociation pm;
  (* Eliminate common subexpressions. *)
  Llvm_scalar_opts.add_gvn pm;
  Llvm_scalar_opts.add_cfg_simplification pm;
  ignore (PassManager.initialize pm);

  (* Compile the Blaise AST down to LLVM IR. *)
  Llvm_be.compile ast state;

  let result =
    let main = match Llvm.lookup_function "main" state#get_module with
      | Some func -> func
      | None ->
          (* No main function cannot happen. *)
          assert false
    in
    ExecutionEngine.run_function
      main [||] (* main takes no argument *)
      exec_engine
  in
    GenericValue.as_int result
